# The packaging information for python cherrypy in XiVO

This repository contains the packaging information for
[python-cherrypy](https://github.com/cherrypy/cherrypy).

To get a new version of python-cherrypy in the XiVO repository, set the
desired version in the `VERSION` file and increment the changelog.

[Jenkins](jenkins.xivo.io) will then retrieve and build the new version.

